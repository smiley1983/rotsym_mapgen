(* rotational symmetric map tiling in OCaml *)

(* rotate a sub-map counter-clockwise by ninety degrees
 *)
let rotate m =
   let rows = Array.length m - 1 in
   let cols = Array.length m.(0) - 1 in
   if not (rows = cols) then 
      failwith "this function cannot rotate non-square arrays\n"
   else
      Array.mapi (fun ir a -> Array.mapi 
        (fun ic c ->
           let new_row = ic in
           let new_col = cols - ir in
              m.(new_row).(new_col)
        ) a) m
;;

(* prints the map to filename with headers
 *)
let print_map m players filename =
   let ochan = open_out filename in
   let rows = Array.length m in
   let cols = Array.length m.(0) in
   output_string ochan (Printf.sprintf "rows %d\ncols %d\nplayers %d" 
         rows cols players);
   Array.iter (fun sm -> output_string ochan "\nm ";
      (Array.iter (fun c -> output_char ochan c) sm)
      ) m;
   output_string ochan "\n";
   close_out ochan
;;

(* blits a onto b with offset off_row, off_col
 *)
let blit_map off_row off_col a b =
   let cols = Array.length a.(0) in
   for count_row = 0 to Array.length a - 1 do
      Array.blit a.(count_row) 0 b.(count_row + off_row) off_col cols
   done
;;

(* tiles maps counter-clockwise from the top left onto a larger map
 *)
let tile_maps size a b c d =
   let m = Array.make_matrix (size * 2) (size * 2) ' ' in
      Algo.blit_map 0 0 a m;
      Algo.blit_map 0 size d m;
      Algo.blit_map size size c m;
      Algo.blit_map size 0 b m;
      m
;;

(* multiplies (without rotation) map a, mr * mc
   so mul_map 1 2 a will return a larger map with two copies of a
   side by side (tiled horizontally) while mul_map 2 1 will tile
   vertically. Used to make a 4 player map into an 8 player map.
 *)
let mul_map mr mc a =
   let rows = Array.length a in
   let cols = Array.length a.(0) in
   let nr = mr * rows in
   let nc = mc * cols in
   let mmap = Array.make_matrix nr nc '.' in
      for row = 0 to (mr - 1) do
         for col = 0 to (mc - 1) do
            Algo.blit_map (row * rows) (col * cols) a mmap
         done
      done;
      mmap
;;

(* character number of the lowercase first letter of the alphabet
 *)
let first_letter = 97;;

(* turn all the starting locations into letters
 *)
let mark_starting_locs m =
   let count = ref (-1) in
   Array.map 
      (Array.map (
         fun f -> if f = '!' then (count := !count + 1;
               char_of_int (first_letter + !count))
            else f
      )) m
;;

let generate_map size players grain carve filename =
   let na = Array.make_matrix size size '%' in
   carve na grain '.';
   Carve.add_player size na;
   let nb = rotate na in
   let nc = rotate nb in
   let nd = rotate nc in
   let tiled = tile_maps size na nb nc nd in
   let mr, mc = if players = 8 then 1, 2 else 1, 1 in
   let mm = mark_starting_locs (mul_map mr mc tiled) in
      print_map mm players filename;
;;

